#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "DataModel.h"
#include <QCompleter>
#include "BookModelBuilder.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {

    ui->setupUi(this);
    setupTableView();
    ui->
}

MainWindow::~MainWindow() {

    delete ui->searchEdit->completer();
    delete ui;
}

void MainWindow::setModel(DataModel *model, const QModelIndex &rootIndex ){

    ui->columnView->setModel(model->getFilterProxy());
    ui->columnView->setRootIndex(rootIndex);

    ui->tableView->setModel(model->getFilterProxy());
    ui->tableView->setRootIndex(rootIndex);


    connect(ui->searchEdit, SIGNAL(textChanged(QString)),
            model->getFilterProxy(), SLOT(setFilterRegExp(QString)) );

    QCompleter *completer = new QCompleter(model->getModelDictionnary());

    completer->setCaseSensitivity(Qt::CaseInsensitive);
    ui->searchEdit->setCompleter(completer);
}

void MainWindow::openSongBook(Book &songBook) {
    DataModel *smodel = (new BookModelBuilder())->build(hardCodedBook);
    setModel(smodel, QModelIndex());
}

void MainWindow::openSong(QString &songFile) {

}


/**
 * @brief MainWindow::setupTableView
 *
 * Setup the table view visual aspect
 *
 */
void MainWindow::setupTableView() {
  ui->tableView->setSortingEnabled(true);
  ui->tableView->setAlternatingRowColors(true);ui->tableView->verticalHeader()->hide();
  ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
  ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
  ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
  // TODO: Check/Uncheck selected lines on [SPACE] key action
}
