#ifndef ACTIONFACTORY_H
#define ACTIONFACTORY_H

#include <QObject>
#include <QAction>


/***
 * This class provides all user (or internally) handled actions
 *
 * These actions are separated and stored in this class in order to:
 *  - wire them in any type of user interface (menu, toolbar button, shortcut event, ...)
 *  - to avoid to hard-code them in MMI code
 *  - to help debugging code by directly accessing the Action to raise
 *
 * */
class ActionFactory : public QObject
{
private :
    QAction *openSongBook;
    QAction *editSongBook;
    QAction *openSong;
    QAction *editSong;

    Q_OBJECT
public:
    explicit ActionFactory(QObject *parent = 0);
    QAction * getOpenSongBook();
    QAction * getEditSongBookAction();

    QAction * getOpenSong();
    QAction * getEditSongBookAction();

signals:

public slots:

};

#endif // ACTIONFACTORY_H
