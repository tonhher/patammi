/********************************************************************************
** Form generated from reading UI file 'FormSongEditor.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORMSONGEDITOR_H
#define UI_FORMSONGEDITOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FormSongEditor
{
public:

    void setupUi(QWidget *FormSongEditor)
    {
        if (FormSongEditor->objectName().isEmpty())
            FormSongEditor->setObjectName(QStringLiteral("FormSongEditor"));
        FormSongEditor->resize(717, 804);

        retranslateUi(FormSongEditor);

        QMetaObject::connectSlotsByName(FormSongEditor);
    } // setupUi

    void retranslateUi(QWidget *FormSongEditor)
    {
        FormSongEditor->setWindowTitle(QApplication::translate("FormSongEditor", "Form", 0));
    } // retranslateUi

};

namespace Ui {
    class FormSongEditor: public Ui_FormSongEditor {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORMSONGEDITOR_H
