#include "MainWindow.h"
#include <QApplication>
#include <QFileSystemModel>

#include "DataModel.h"
//#include "BookModelBuilder.h"
#include "SessionManager.h"

#include "Logger.h"
#include <ConsoleAppender.h>


// FOR DEBUG ONLY -----------
#include "SongFactory.h"
#include "Song.h"
void debug(void);
// ---------------------------

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    MainWindow *window = new MainWindow();
    SessionManager *manager = new SessionManager();

    ConsoleAppender* consoleAppender = new ConsoleAppender;
   // consoleAppender->setFormat("[%{type:-7}] <%{Function}> %{message}\n");
    logger->registerAppender(consoleAppender);
    LOG_INFO("Starting the application");

    connect(manager, SIGNAL(bookOpened(Book *)),
            window, SLOT(openSongBook(Book *)) );

    /** For debug: automatically open a SongBook */
    Book hardCodedBook;
    hardCodedBook.setDataDir("/home/tonio/workspace/SongBook/songs");
    manager.openBook(&hardCodedBook);
    /**** */

    w.show();
    return a.exec();


    /****
     * TODO application:
     *  - read/save book using json format
     *  - read/save book preferences
     *  - parse a directory to identify existing songs
     * ****/

    /***
     * TODO archi:
     * - status bar messages, make console output
     *
     *
     * */


    /****
     * TODO MMI:
     *  - getSelectedSongs
     *  - pop-up menu
     *  - action on click
     *  - status bar
     *  - Chords dock with predefined standards chords
     * ***/

    /****
     * TODO application:
     *  - read/save book using json format
     *  - read/save book preferences
     *  - parse a directory to identify existing songs
     * ****/

    /****
     * TODO improvment:
     *  - wire glyrc to download cover, lyric
     *  - wire frescobaldi
     * ***/

    /****
     * TODO improvment:
     *  - set-up a detacheableQtabWidget
     *  - wire frescobaldi
     * ***/
}


