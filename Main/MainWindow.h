#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSortFilterProxyModel>
#include "DataModel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setModel( DataModel *model, const QModelIndex &rootIndex );

public slots:
    void openSongBook(Book &songBook);
    void openSong(QString &songFile);

private:
    Ui::MainWindow *ui;
    void setupTableView();
};

#endif // MAINWINDOW_H
