#-------------------------------------------------
#
# Project created by QtCreator 2016-04-28T00:35:14
#
#-------------------------------------------------


SOURCES += main.cpp\
        MainWindow.cpp \
    ActionFactory.cpp \
    SessionManager.cpp

HEADERS  += MainWindow.h \
    ActionFactory.h \
    SessionManager.h

FORMS    += MainWindow.ui

QMAKE_CXXFLAGS += -std=c++11
