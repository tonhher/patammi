#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QStandardItemModel>
#include <QSortFilterProxyModel>

// TODO : REname to DataModel
/***
 * This class provides a standard item Model with an integrated sort and filter proxy
 *
 * **/
class DataModel: public QStandardItemModel
{
public:

    DataModel(QObject *parent=0);
    DataModel( int rows, int columns, QObject *parent=0);


    QSortFilterProxyModel * getFilterProxy( );

    /***
     * Provides Model list of all words/sentences.
     *
     * Usefull for completion
     *
     * Note that this list is not updated on model added/removed items actions
     *
     * */
    QStringList getModelDictionnary( );

private :
    QSortFilterProxyModel *sortModelProxy ;

};

#endif // DATAMODEL_H
