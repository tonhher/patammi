#include "DataModel.h"
#include <QDebug>
#include <QMetaProperty>

DataModel::DataModel(QObject *parent) :
    QStandardItemModel(parent) {
    sortModelProxy = NULL;
}

DataModel::DataModel(int rows, int columns, QObject *parent):
    QStandardItemModel(rows, columns, parent) {
    sortModelProxy = NULL;
}


QSortFilterProxyModel * DataModel::getFilterProxy( ) {
    if( sortModelProxy == NULL ) {
        sortModelProxy = new QSortFilterProxyModel(this);
        sortModelProxy->setSourceModel(this);
        sortModelProxy->setFilterKeyColumn(-1);
        sortModelProxy->setDynamicSortFilter(true);
    }
    return sortModelProxy;
}

QStringList DataModel::getModelDictionnary() {
    QStringList *dictionnary = new QStringList();

    for( int row = 0 ; row < rowCount(); row++ ) {
        for( int column =0 ; column < columnCount(); column++ ) {
            QStandardItem *anItem = item(row, column);
            QVariant itemData = anItem->data(Qt::DisplayRole);
            QString words = itemData.toString();
            if( words!=NULL && !words.isEmpty()) {
                dictionnary->append(words);
            }
            if( words.contains(" ")) {
                dictionnary->append(words.split(" "));
            }
        }
    }
    dictionnary->removeDuplicates();
    return *dictionnary;
}

