#-------------------------------------------------
#
# Project created by QtCreator 2016-04-30T16:00:22
#
#-------------------------------------------------

QT       += gui

TARGET = model
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    DataModel.cpp

HEADERS += \
    DataModel.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

QMAKE_CXXFLAGS += -std=c++11
