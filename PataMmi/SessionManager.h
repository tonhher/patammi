#ifndef SESSIONMANAGER_H
#define SESSIONMANAGER_H

#include <QObject>
#include "ActionFactory.h"
#include "Book.h"
#include "Song.h"

/***
 * This class is in charge of managing a songbook client Session by:
 *  - driving MMI
 *  - launching action (loading, update, external tool launching...)
 *
 * Note:
 *  This class intend to be usable even without GUI launched
 *
 * */

class SessionManager : public QObject
{
    Q_OBJECT
private:
    Book *book;
    const ActionFactory *factory;

public:

    explicit SessionManager(ActionFactory *factory, QObject *parent = 0);

   void openBook(Book *songBook) ;
   void saveBook();
   void closeBook();
   void addSong(Song *newSong);
   void removeSong(Song *deprecatedSong);

   void openSong(Song *song);


   /**
    * Build the book
    * */
   void make();

signals:
   void bookOpened(Book *songBook);
   void bookClosed(Book *songBook);
   void songAdded(Song *newSong);
   void songRemoved(Song *deprecatedSong);


public slots:

};

#endif // SESSIONMANAGER_H
