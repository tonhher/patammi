#ifndef SONGDOCUMENT_H
#define SONGDOCUMENT_H

#include "Song.h"

class SongDocument
{
public:
    SongDocument(Song *song);



private:
    Song *songProperties;
};

#endif // SONGDOCUMENT_H
