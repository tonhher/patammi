#ifndef BOOK_H
#define BOOK_H


#include <QObject>
#include <QJsonObject>
//#include "Enum.h"



class Book : public QObject
{
    Q_OBJECT
public:
    explicit Book(QObject *parent = 0);

    /**
     * Directory holding patadata
     *
     * @brief getDataDir
     * @return
     */
    const QString getDataDir() const;
    void setDataDir(const QString &datadir);

    bool getDiagram() const;
    void setDiagram(const bool &value);

    bool getImportantDiagramOnly() const;
    void setImportantDiagramOnly(const bool &value);

    bool getLilypond() const;
    void setLilypond(const bool &value);

    bool getPictures() const;
    void setPictures(const bool &value);

    bool getTabs() const;
    void setTabs(const bool &value);

    bool getRepeatChords() const;
    void setRepeatChords(const bool &value);

    bool getOneSongPerPage() const;
    void setOneSongPerPage(const bool &value);

    bool getGuitar() const;
    void setGuitar(const bool &value);

    bool getUkulele() const;
    void setUkulele(const bool &value);

    QString getLang() const;
    void setLang(const QString &value);

    QString getVersion() const;
    void setVersion(const QString &value);

    QString getSubtitle() const;
    void setSubtitle(const QString &value);

    QString getWeb() const;
    void setWeb(const QString &value);

    QString getEmail() const;
    void setEmail(const QString &value);

    QString getPicturePath() const;
    void setPicturePath(const QString &value);

    QString getCopyright() const;
    void setCopyright(const QString &value);

    QString getFooter() const;
    void setFooter(const QString &value);

    QString getLicensePath() const;
    void setLicensePath(const QString &value);

    QString getFontSize() const;
    void setFontSize(const QString &value);

    QString getNumberShade() const;
    void setNumberShade(const QString &value);

    QString getNoteShade() const;
    void setNoteShade(const QString &value);

    QString getAuthor() const;
    void setAuthor(const QString &value);

    QString getBookTemplate() const;
    void setBookTemplate(const QString &value);

    QString getTitle() const;
    void setTitle(const QString &value);

    QString getType() const;
    void setType(const QString &value);

    QStringList getSongs() const;
    void setSongs(QStringList &songs);

signals:

public slots:

private:

    /*** Book Collection parameters ***/
    QString datadir;
    QString bookTemplate;
    QString title;
    QString author;
    QString type;
    /*** -------------------------- ***/

    /*** Advanced options           ***/
    QString lang; // Book language for chords names and all but songs texts
    QString version; // patacrep version
    QString subtitle;
    QString web;
    QString email;
    QString picturePath;
    QString copyright;
    QString footer;
    QString licensePath;
    QString fontSize;
    QString numberShade;
    QString noteShade;
    /*** -------------------------- ***/


    /*** Book Options ***/
    bool diagram;
    bool importantDiagramOnly;
    bool lilypond;
    bool pictures;
    bool tabs;
    bool repeatChords;
    bool oneSongPerPage;
    /*** ----------- ***/

    /*** Instrument Option ***/
    bool guitar;
    bool ukulele;
    /*** ---------------- ***/



};


#endif // BOOK_H
