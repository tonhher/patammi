#include "LatexUtils.h"

#include <QRegExp>

LatexUtils::LatexUtils() {
}

QString LatexUtils::toUtf8(const QString &str) {
    QString result(str);
    result.replace(QRegExp("([^\\\\])~"), QString("\\1%1").arg(QChar(QChar::Nbsp)));
    result.replace(QRegExp("\\\\([&~])"), "\\1");
    result.replace(QRegExp("\\{?\\\\l?dots\\}?"),  "...");
    result.replace("\\%",  "%");
    return result;
}

QString LatexUtils::fromUtf8(const QString &str) {
    QString result(str);
    result.replace(QRegExp("([&~])"), "\\\\1");
    result.replace(QChar(QChar::Nbsp), "~");
    result.replace("...", "\\dots");
    result.replace("%", "\\%");
    return result;
}
