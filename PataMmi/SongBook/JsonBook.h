#ifndef JSONBOOK_H
#define JSONBOOK_H

#include "Book.h"
#include <QJsonObject>
#include <QFile>

namespace BookParametersConfigKeyNS {
    const QString AUTHOR("author");
    const QString TYPE("booktype");
    const QString FOOTER("footer");

    const QString INSTRUMENTS("instruments");
    const QString GUITAR("guitar");
    const QString UKULELE("ukulele");

    const QString LANG("lang");
    const QString LICENSE("license");
    const QString EMAIL("mail");
    const QString MAIN_FONT_SIZE("mainfontsize");
    const QString PICTURE("picture");
    const QString PICTURE_COPYRIGHT("picturecopyright");
    const QString INDEX_BG_COLOR("indexbgcolor");
    const QString NOTE_BG_COLOR("indexbgcolor");
    const QString SONG_NB_BG_COLOR("songnumberbgcolor");
    const QString TITLE("title");
    const QString SUBTITLE("subtitle");
    const QString VERSION("version");
    const QString WEB_SITE("web");
    const QString SONGS("songs");

    const QString BOOK_OPTIONS("bookoptions");
    const QString SHOW_CHORDS_DIAGRAM("diagram");
    const QString SHOW_CHORDS_IMPORTANT_DIAGRAM("importantdiagramonly");
    const QString SHOW_LILYPOND("lilypond");
    const QString SHOW_PICTURES("pictures");
    const QString SHOW_TABS("tabs");
    const QString SHOW_REPEATED_CHORDS("repeatchords");
    const QString SHOW_ONE_SONG_PER_PAGE("onesongperpage");



    const QString TEMPLATE("template");
    const QString DATADIR = QStringLiteral("datadir");


}

class JsonBook : public Book {
public:
    explicit JsonBook(QObject *parent = 0);
    explicit JsonBook(QFile &jsonFile, QObject *parent = 0);

    void loadFromJson(QFile &jsonFile);
    void saveToJson(QFile jsonFile);

private :
        QJsonObject jsonContent;
};


#endif // JSONBOOK_H
