#include <QDirIterator>

#include "SongFactory.h"
#include "Song.h"
#include "Logger.h"
#include "LatexUtils.h"

const QRegExp reSgFile("(.*)\\\\begin\\{?song\\}?\\{([^\\}]+)\\}[^[]*\\[([^]]*)\\](.*)\\s*\\\\endsong(.*)");
const QRegExp reArtist("by=\\{?([^,\\{\\}]+)");
const QRegExp reAlbum("album=\\{?([^,\\{\\}]+)");
const QRegExp reOriginalSong("original=\\{?([^,\\{\\}]+)");
const QRegExp reUrl("url=\\{?([^,\\{\\}]+)");
const QRegExp reCoverName("cov=\\{?([^,\\{\\}]+)");
const QRegExp reLilypond("\\\\lilypond");
const QRegExp reLanguage("\\\\selectlanguage\\{([^\\}]+)");
const QRegExp reColumnCount("\\\\songcolumns\\{([^\\}]+)");
const QRegExp reCapo("\\\\capo\\{([^\\}]+)");
const QRegExp reTranspose("\\\\transpose\\{([^\\}]+)");
const QRegExp reCover("\\\\cover");

SongFactory::SongFactory(QObject *parent) :
    QObject(parent) {
}

Song* SongFactory::parse(QFile &songFile) {
    if (!songFile.open(QIODevice::ReadOnly | QIODevice::Text))     {
        LOG_WARNING() << "Song::fromFile: unable to open " << songFile.fileName();
        return NULL;
    }

    QTextStream stream (&songFile);
    stream.setCodec("UTF-8");
    QString fileContent = stream.readAll();
    songFile.close();

    return parse(songFile, fileContent);
}

Song *SongFactory::parse(QFile &songFile, QString &songFileContent)  {

    reSgFile.indexIn(songFileContent);
    QString prefix = reSgFile.cap(1);
    reLanguage.indexIn(prefix);
    QString options = reSgFile.cap(3);
    reArtist.indexIn(options);
    reAlbum.indexIn(options);
    reOriginalSong.indexIn(options);
    reCoverName.indexIn(options);

    QString content = reSgFile.cap(4);
    QString post = reSgFile.cap(5);

    QString title = LatexUtils::toUtf8(reSgFile.cap(2));
    QString artist = LatexUtils::toUtf8(reArtist.cap(1));
    QString album = LatexUtils::toUtf8(reAlbum.cap(1));
    //QString originalSong = latexToUtf8(reOriginalSong.cap(1));
    QString albumCover = reCoverName.cap(1);
    QFile relativeFile(songFile.fileName());
    QString path = songFile.fileName();
    int i = path.lastIndexOf("/songs/");
    if( i != -1 ) {
        relativeFile.setFileName(path.right( path.length() - i - 7));
    }
    QString lang = reLanguage.cap(1);
    QString year = "NYI";
    Song * result = new Song(relativeFile,
                             title,
                             artist,
                             album,
                             lang,
                             year, this);
    return result;

}

QList<Song *> SongFactory::parse(QDir songsDirectory) {
    QList<Song *> result;
    if( !songsDirectory.exists()) {
        LOG_WARNING() << "SongFactory: unable to parse diretory " << songsDirectory ;
        return result;
    }
    //TODO Use a QDirIterator..
    QStringList nameFilter("*.sg");
    QDirIterator findSongFiles(songsDirectory.absolutePath(), nameFilter, QDir::Files, QDirIterator::Subdirectories | QDirIterator::FollowSymlinks);
    while( findSongFiles.hasNext()) {
        QFile songFile(findSongFiles.next());
        LOG_DEBUG("Loading " +  songFile.fileName());
        result.append( parse(songFile));
    }
    return result;
}
