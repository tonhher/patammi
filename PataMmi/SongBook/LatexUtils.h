#ifndef LATEXUTILS_H
#define LATEXUTILS_H

#include <QString>



/*!
 * This class provides some useful utilities related with latex files content handling
 *
 **/
class LatexUtils
{
public:
    LatexUtils();


    /*!
      Converts LaTeX special sequences to utf8 characters.
      For example: \'e -> é.
      \sa utf8ToLatex
    */
    static QString toUtf8(const QString & str) ;

    /*!
      Converts some utf8 characters to LaTeX sequences.
      For example: & -> \&.
      \sa latexToUtf8
    */
    static QString fromUtf8(const QString & str) ;
};

#endif // LATEXUTILS_H
