#include "Book.h"



Book::Book(QObject *parent) :
    QObject(parent) {
}

const QString Book::getDataDir() const
{
    return datadir;
}

void Book::setDataDir(const QString &datadir)
{
    this->datadir = datadir;
}



bool Book::getDiagram() const
{
    return diagram;
}

void Book::setDiagram(const bool &value)
{
    diagram = value;
}
bool Book::getImportantDiagramOnly() const
{
    return importantDiagramOnly;
}

void Book::setImportantDiagramOnly(const bool &value)
{
    importantDiagramOnly = value;
}
bool Book::getLilypond() const
{
    return lilypond;
}

void Book::setLilypond(const bool &value)
{
    lilypond = value;
}
bool Book::getPictures() const
{
    return pictures;
}

void Book::setPictures(const bool &value)
{
    pictures = value;
}
bool Book::getTabs() const
{
    return tabs;
}

void Book::setTabs(const bool &value)
{
    tabs = value;
}
bool Book::getRepeatChords() const
{
    return repeatChords;
}

void Book::setRepeatChords(const bool &value)
{
    repeatChords = value;
}
bool Book::getOneSongPerPage() const
{
    return oneSongPerPage;
}

void Book::setOneSongPerPage(const bool &value)
{
    oneSongPerPage = value;
}
bool Book::getGuitar() const
{
    return guitar;
}

void Book::setGuitar(const bool &value)
{
    guitar = value;
}
bool Book::getUkulele() const
{
    return ukulele;
}

void Book::setUkulele(const bool &value)
{
    ukulele = value;
}
QString Book::getLang() const
{
    return lang;
}

void Book::setLang(const QString &value)
{
    lang = value;
}
QString Book::getVersion() const
{
    return version;
}

void Book::setVersion(const QString &value)
{
    version = value;
}
QString Book::getSubtitle() const
{
    return subtitle;
}

void Book::setSubtitle(const QString &value)
{
    subtitle = value;
}
QString Book::getWeb() const
{
    return web;
}

void Book::setWeb(const QString &value)
{
    web = value;
}
QString Book::getEmail() const
{
    return email;
}

void Book::setEmail(const QString &value)
{
    email = value;
}
QString Book::getPicturePath() const
{
    return picturePath;
}

void Book::setPicturePath(const QString &value)
{
    picturePath = value;
}
QString Book::getCopyright() const
{
    return copyright;
}

void Book::setCopyright(const QString &value)
{
    copyright = value;
}
QString Book::getFooter() const
{
    return footer;
}

void Book::setFooter(const QString &value)
{
    footer = value;
}
QString Book::getLicensePath() const
{
    return licensePath;
}

void Book::setLicensePath(const QString &value)
{
    licensePath = QString(value);
}
QString Book::getFontSize() const
{
    return fontSize;
}

void Book::setFontSize(const QString &value)
{
    fontSize = value;
}
QString Book::getNumberShade() const
{
    return numberShade;
}

void Book::setNumberShade(const QString &value)
{
    numberShade = value;
}
QString Book::getNoteShade() const
{
    return noteShade;
}

void Book::setNoteShade(const QString &value)
{
    noteShade = value;
}
QString Book::getAuthor() const
{
    return author;
}

void Book::setAuthor(const QString &value)
{
    author = value;
}
QString Book::getBookTemplate() const
{
    return bookTemplate;
}

void Book::setBookTemplate(const QString &value)
{
    bookTemplate = value;
}
QString Book::getTitle() const
{
    return title;
}

void Book::setTitle(const QString &value)
{
    title = value;
}
QString Book::getType() const
{
    return type;
}

void Book::setType(const QString &value)
{
    type = value;
}

























