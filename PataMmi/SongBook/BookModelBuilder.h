#ifndef BOOKMODELBUILDER_H
#define BOOKMODELBUILDER_H


#include <QObject>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include "Song.h"
#include "DataModel.h"
#include "Book.h"



/***
 * This class is in charge of building the data model holding the book song list
 *
 *
 * */
class BookModelBuilder : public QObject
{
    Q_OBJECT
public:
    explicit BookModelBuilder(QObject *parent = 0);

    DataModel *build(Book *songBook);

signals:

public slots:


private :
    QList<QStandardItem *> buildRow(const Song * const song);
    DataModel * setupModel();

};

#endif // BOOKMODELBUILDER_H
