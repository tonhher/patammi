#include "JsonBook.h"
#include <QDir>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

#include "Logger.h"

using namespace BookParametersConfigKeyNS;

JsonBook::JsonBook(QObject *parent) : Book(parent) {

}


JsonBook::JsonBook(QFile &jsonFile, QObject *parent)  : Book(parent) {
    loadFromJson(jsonFile);
}


void JsonBook::loadFromJson(QFile &jsonFile)
{
    if (!jsonFile.exists() ) {
        LOG_ERROR("File does not exists: " + jsonFile.fileName());
    } else if (!jsonFile.open(QIODevice::ReadOnly)) {
        LOG_ERROR("File can not be opened: "+ jsonFile.fileName());
    }

    QByteArray fileContent = jsonFile.readAll();
    jsonContent = QJsonDocument(QJsonDocument::fromJson(fileContent)).object();

    setTitle(jsonContent[TITLE].toString());
    setAuthor(jsonContent[AUTHOR].toString());
    setType(jsonContent[TYPE].toString());

    setLang(jsonContent[LANG].toString());
    setVersion(jsonContent[VERSION].toString());
    setSubtitle(jsonContent[SUBTITLE].toString());
    setWeb(jsonContent[WEB_SITE].toString());
    setEmail(jsonContent[EMAIL].toString());
    setPicturePath(jsonContent[PICTURE].toString());
    setCopyright(jsonContent[PICTURE_COPYRIGHT].toString());
    setFooter(jsonContent[FOOTER].toString());
    setLicensePath(jsonContent[LICENSE].toString());
    setFontSize(jsonContent[MAIN_FONT_SIZE].toString());
    setNumberShade(jsonContent[SONG_NB_BG_COLOR].toString());
    setNoteShade(jsonContent[NOTE_BG_COLOR].toString());
    QDir jsonDataDir = QDir(jsonContent[DATADIR].toString());
    QDir resultDataDir = jsonDataDir;
    if( !jsonDataDir.isAbsolute()) {
        resultDataDir = QDir(jsonFile.fileName());
        resultDataDir.cdUp();
        resultDataDir.cd(jsonDataDir.path());
    }
    if(resultDataDir.exists()) {
        setDataDir(resultDataDir.absolutePath());
    }

    QJsonArray bookOptions = jsonContent[BOOK_OPTIONS].toArray();
    setDiagram(bookOptions.contains(
        QJsonValue(SHOW_CHORDS_DIAGRAM)));
    setImportantDiagramOnly(
        bookOptions.contains(QJsonValue(SHOW_CHORDS_IMPORTANT_DIAGRAM)));
    setLilypond(
        bookOptions.contains(QJsonValue(SHOW_LILYPOND)));
    setPictures(
        bookOptions.contains(QJsonValue(SHOW_PICTURES)));
    setTabs(
        bookOptions.contains(QJsonValue(SHOW_TABS)));
    setRepeatChords(
        bookOptions.contains(QJsonValue(SHOW_REPEATED_CHORDS)));
    setOneSongPerPage(
        bookOptions.contains(QJsonValue(SHOW_ONE_SONG_PER_PAGE)));

    QJsonArray instruments = jsonContent[INSTRUMENTS].toArray();
    setGuitar(instruments.contains(QJsonValue(GUITAR)));
    setUkulele(instruments.contains(QJsonValue(UKULELE)));
}

void JsonBook::saveToJson(QFile jsonFile) {

    jsonContent[TITLE] = getTitle();
    jsonContent[AUTHOR] = getAuthor();
    jsonContent[TYPE] = getType();

    jsonContent[LANG] = getLang();
    jsonContent[VERSION] = getVersion();
    jsonContent[SUBTITLE] = getSubtitle();
    jsonContent[WEB_SITE] = getWeb();
    jsonContent[EMAIL] = getEmail();
    jsonContent[PICTURE] = getPicturePath();
    /***
     * jsonContent[PICTURE_COPYRIGHT] = getPicture
    setFooter(jsonContent[FOOTER].toString());
    setLicensePath(jsonContent[LICENSE].toString());
    setFontSize(jsonContent[MAIN_FONT_SIZE].toString());
    setNumberShade(jsonContent[SONG_NB_BG_COLOR].toString());
    setNoteShade(jsonContent[NOTE_BG_COLOR].toString());

    QJsonArray bookOptions = jsonContent[BOOK_OPTIONS].toArray();
    setDiagram(bookOptions.contains(
        QJsonValue(SHOW_CHORDS_DIAGRAM)));
    setImportantDiagramOnly(
        bookOptions.contains(QJsonValue(SHOW_CHORDS_IMPORTANT_DIAGRAM)));
    setLilypond(
        bookOptions.contains(QJsonValue(SHOW_LILYPOND)));
    setPictures(
        bookOptions.contains(QJsonValue(SHOW_PICTURES)));
    setTabs(
        bookOptions.contains(QJsonValue(SHOW_TABS)));
    setRepeatChords(
        bookOptions.contains(QJsonValue(SHOW_REPEATED_CHORDS)));
    setOneSongPerPage(
        bookOptions.contains(QJsonValue(SHOW_ONE_SONG_PER_PAGE)));

    QJsonArray instruments = jsonContent[INSTRUMENTS].toArray();
    setGuitar(instruments.contains(QJsonValue(GUITAR)));
    setUkulele(instruments.contains(QJsonValue(UKULELE)));
    ***/
    // TODO save the JSON file!!!
}
