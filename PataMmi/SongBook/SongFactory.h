#ifndef SONGFACTORY_H
#define SONGFACTORY_H

#include "Song.h"
#include <QList>
#include <QDir>

/***
 * Parse and extract info from an existing song file (.sg)
 *
 * */
class SongFactory : public QObject
{
        Q_OBJECT
public:
    SongFactory(QObject *parent = 0);
    Song *parse(QFile &songFile);
    QList<Song*> parse(QDir songsDirectory) ;

private :
    Song *parse(QFile &songFile, QString &songFileContent);
};

#endif // SONGFACTORY_H
