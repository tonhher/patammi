#include "BookModelBuilder.h"
#include <QSortFilterProxyModel>
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDir>

#include "Song.h"
#include "SongFactory.h"
#include "DataModel.h"
#include "JsonBook.h"

#include <Logger.h>


// TODO: Rename to SongsModelBuilder
BookModelBuilder::BookModelBuilder(QObject *parent) :
    QObject(parent)
{
}

DataModel *BookModelBuilder::build(Book *songBook) {
    DataModel *model = setupModel();

    QString datadir = songBook->getDataDir();
    if ( QDir(datadir).exists() ) {
        SongFactory *factory = new SongFactory(songBook);
        QList<Song *> songs = factory->parse(datadir);

        for( Song *song : songs) {
            model->appendRow(buildRow(song));
        }
    }

    return model;
}

QList<QStandardItem *> BookModelBuilder::buildRow(const Song * const song)
{

    QList<QStandardItem *> songData = QList<QStandardItem *> ();

    QStandardItem *enableData = new QStandardItem(  );
    enableData->setCheckable(song->isEnable());

    songData.append( enableData );
    QStandardItem *titleData = new QStandardItem( song->getTitle() );
    songData.append( titleData);
    songData.append( new QStandardItem( song->getSinger() ));
    songData.append( new QStandardItem( song->getAlbum() ));
    songData.append( new QStandardItem( song->getLang() ));
    songData.append( new QStandardItem( song->getYear() ));
    songData.append( new QStandardItem( song->getFile()->fileName() ));
    new QStandardItem( song->getYear() );
    return songData;

}

DataModel *BookModelBuilder::setupModel() {
     DataModel *model = new DataModel(0 , 6, this);
     int i =0;
     model->setHeaderData(i++, Qt::Horizontal,  "");
     model->setHeaderData(i++, Qt::Horizontal,  "Title");
     model->setHeaderData(i++, Qt::Horizontal,  "Singer");
     model->setHeaderData(i++, Qt::Horizontal,  "Album");
     model->setHeaderData(i++, Qt::Horizontal,  "Lang");
     model->setHeaderData(i++, Qt::Horizontal,  "Year");
     model->setHeaderData(i++, Qt::Horizontal,  "File");

     return model;
}

