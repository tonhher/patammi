#include "Song.h"




// Add required properties
Song::Song(const QFile &songFile,
        const QString title, const QString singer, const QString album, const QString lang,
                   const QString year, QObject *parent)
    :  QObject(parent), title(title),  singer(singer), album(album), lang(lang), year(year) {
    file.setFileName(songFile.fileName());
    enable = true;
}

const QString Song::getTitle() const {
    return title;
}

const QString Song::getSinger() const {
    return singer;
}


const QString Song::getAlbum() const {
    return album;
}

const QFile* Song::getFile() const {
    return &file;
}

const QString Song::getLang() const {
    return lang;
}

const QString Song::getYear() const {
    return year;
}

bool Song::isEnable() const {
    return enable;
}

void Song::setEnable(bool isEnable) {
    this->enable = isEnable;
}
