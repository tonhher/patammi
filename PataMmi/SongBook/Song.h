#ifndef SONGINFO_H
#define SONGINFO_H

#include <QObject>
#include <QFile>
#include <QIcon>

/***
 * This class holds a song data.
 *
 * It should not be used to create, update or modify a song file
 *
 *
 * */
class Song : public QObject
{
    Q_OBJECT
    // TODO: check if defining property is really required or useless!
   Q_PROPERTY(const QString title READ getTitle)
   Q_PROPERTY(const bool enable READ isEnable WRITE setEnable)
 //   Q_PROPERTY(const QString singer READ getSinger)


 public :  static const QString * InfosToString;
private:

    const QString title;
    const QString singer;
    const QString album;
    const QString lang;
    const QString year;
    /**
     * @brief file File holding the song (.sg file) relative to datadir
     */
    QFile file;
    const QIcon albumCoverIcon;
    bool enable;

    // TODO: add support for
    //  bool hasLylipond;
    //  QString originalSong


public:
    Song(const QFile &songFile,  const QString title, const QString singer, const QString album, const QString lang,
             const QString year, QObject *parent = NULL);

    const QString getTitle() const;
    const QString getSinger() const;
    const QString getLang() const;
    const QString getYear() const;

    const QString getAlbum() const;
    const QFile *getFile() const;
    bool isEnable() const;
    void setEnable(bool isEnable);


signals:

public slots:

};

#endif // SONGINFO_H
