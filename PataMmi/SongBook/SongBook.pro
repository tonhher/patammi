#-------------------------------------------------
#
# Project created by QtCreator 2016-04-28T01:01:47
#
#-------------------------------------------------

SOURCES += Song.cpp \
    SongFactory.cpp \
    LatexUtils.cpp \
    Book.cpp \
    JsonBook.cpp \
    BookModelBuilder.cpp

HEADERS += Song.h \
    SongFactory.h \
    LatexUtils.h \
    Book.h \
    JsonBook.h \
    BookModelBuilder.h
