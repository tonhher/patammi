#include "MenuBuilder.h"
#include <QObject>
using namespace ActionsNameNS;

static QString const SEPARATOR("separator");



MenuBuilder::MenuBuilder(ActionFactory *actionFactory, QMenuBar *menuBar, QWidget *parent)
    :actionFactory(actionFactory), menuBar(menuBar)
{
}

void MenuBuilder::build()
{
    buildFileMenu();
    buildEditMenu();
    buildAboutMenu();
}


/**** PRIVATE
 *
 * *******************************/

void MenuBuilder::buildMenu(QString actionPrefix, QStringList menuActions)
{
    QMenu *menu = new QMenu(actionPrefix, menuBar);
    menu->addSeparator();

    for(QString actionToAddInMenu: menuActions) {
        if(SEPARATOR.compare(actionToAddInMenu) == 0) {
            menu->addSeparator();
        } else {
            menu->addAction(
                actionFactory->getAction(actionToAddInMenu));
        }
    }

    menuBar->addAction(menu->menuAction());
}

void MenuBuilder::buildFileMenu()
{
    QStringList menuContent = QStringList()
            << DOCUMENT_NEW << DOCUMENT_OPEN << DOCUMENT_SAVE
            << SEPARATOR
            << DOCUMENT_OPEN_RECENT_1 << DOCUMENT_OPEN_RECENT_2
            << DOCUMENT_OPEN_RECENT_3 << DOCUMENT_OPEN_RECENT_4
            << SEPARATOR
            << APPLICATION_EXIT;

    buildMenu(QObject::tr("&File"), menuContent);

    // TODO : Try to setup a sub-menu for opening recent files
//    QMenu *menuRecent = new QMenu("Open recent", menuBar);
//    menuRecent->addSeparator();
//    for(QAction recentAction : actionFactory->getOpenRecentFileActions()) {
//        menuRecent->addAction(recentAction);
//    }
//    actionFactory->getAction(DOCUMENT_OPEN_RECENT)
}

void MenuBuilder::buildEditMenu()
{

    QStringList menuContent = QStringList()
            << EDIT_CLEAR << EDIT_SELECT_ALL ;
    buildMenu(QObject::tr("&Edit"), menuContent);
}

void MenuBuilder::buildAboutMenu()
{
    QStringList menuContent = QStringList()
            << HELP_ABOUT << HELP_ABOUT_QT ;
    buildMenu(QObject::tr("&About"), menuContent);
}
