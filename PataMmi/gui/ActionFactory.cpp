#include "ActionFactory.h"

#include <QSettings>
#include <QFileInfo>
#include <QApplication>
#include <QKeySequence>
#include <QRegularExpression>

using namespace ActionsNameNS;





ActionFactory::ActionFactory(QObject *parent) :
    QObject(parent) {
    actions = QList<QAction *>();
}

void ActionFactory::initialize()
{
    createAllActions();
    createConnections();
    updateRecentFilesActions();
}

void ActionFactory::addAction(QString id, QAction *actionToAdd)
{
    actionToAdd->setObjectName(id);
    actions.append(actionToAdd);
}



typedef struct  {
    QString id;
    QString name;
    QString tooltip;
    QString shortcut;

} ActionsDefinitions;

static  ActionsDefinitions const definitions[] = {
    { .id = APPLICATION_EXIT,
        .name         = QStringLiteral("&Quit"),
        .tooltip      = QStringLiteral("Exit application"),
        .shortcut     = QStringLiteral("Ctrl+Q"),
    },
    { .id = DOCUMENT_NEW,
        .name         = QStringLiteral("&New"),
        .tooltip      = QStringLiteral("Create a new document"),
        .shortcut     = QStringLiteral("Ctrl+N")
    },
    { .id = DOCUMENT_OPEN,
        .name         = QStringLiteral("&Open"),
        .tooltip      = QStringLiteral("Open an existing document"),
        .shortcut     = QStringLiteral("Ctrl+O")
    },
    { .id = DOCUMENT_OPEN_RECENT + "#1",
        .name         = QStringLiteral("Open recent 1"),
        .tooltip      = QStringLiteral("Open last recent document"),
        .shortcut     = QStringLiteral("Ctrl+1")
    },
    { .id = DOCUMENT_OPEN_RECENT + "#2",
        .name         = QStringLiteral("Open recent 2"),
        .tooltip      = QStringLiteral("Open a recent document"),
        .shortcut     = QStringLiteral("Ctrl+2")
    },
    { .id = DOCUMENT_OPEN_RECENT + "#3",
        .name         = QStringLiteral("Open recent 3"),
        .tooltip      = QStringLiteral("Open a recent document"),
        .shortcut     = QStringLiteral("Ctrl+3")
    },
    { .id = DOCUMENT_OPEN_RECENT + "#4",
        .name         = QStringLiteral("Open recent 4"),
        .tooltip      = QStringLiteral("Open a recent document"),
        .shortcut     = QStringLiteral("Ctrl+4")
    },
    { .id = DOCUMENT_SAVE,
        .name         = QStringLiteral("&Save"),
        .tooltip      = QStringLiteral("Save current document"),
        .shortcut     = QStringLiteral("Ctrl+S")
    },
    { .id = HELP_ABOUT,
        .name         = QStringLiteral("&About"),
        .tooltip      = QStringLiteral("Application's about information"),
        .shortcut     = QStringLiteral("Ctrl+?")
    },
    { .id = HELP_ABOUT_QT,
         .name         = QStringLiteral("About Q&t"),
         .tooltip      = QStringLiteral("QT's about information"),
         .shortcut     = QStringLiteral("Ctrl+T")
     },
    { .id = EDIT_SELECT_ALL,
          .name         = QStringLiteral("Toggle select all"),
          .tooltip      = QStringLiteral("Select or unselect all items"),
          .shortcut     = QStringLiteral("Ctrl+A")
    },
};

void ActionFactory::createAllActions() {

    ActionsDefinitions const *definition = definitions;
    int definitionsSize = sizeof(definitions)/sizeof(ActionsDefinitions);
    while( definitionsSize-- > 0) {
        QString iconfile = definition->id;
        QRegularExpression ee(QString("#\\d+$"), QRegularExpression::NoPatternOption);
        iconfile.remove(ee);
        actions.append(
            createAction(
                definition->id,
                definition->name,
                QIcon::fromTheme(iconfile, QIcon(":/icons/" + iconfile)),
                QKeySequence(definition->shortcut),
                definition->tooltip));
        definition++;
    }

    QAction *recentsFile = getAction(DOCUMENT_OPEN_RECENT);
}

void ActionFactory::createConnections()
{
    connect(getAction(HELP_ABOUT_QT), SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(getAction(APPLICATION_EXIT), SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));
}

QAction *ActionFactory::createAction(
        const QString &id,
        const QString &text,
        const QIcon icon,
        const QKeySequence &shortcut, const QString &tooltip) {
    QAction * action = new QAction(icon, text, this->parent());
    action->setObjectName(id);
    action->setShortcut(shortcut);
    action->setStatusTip(tooltip);
    action->setEnabled(true);

    return action;
}

void ActionFactory::updateRecentFilesActions()
{
    QSettings settings;
    QStringList files = settings.value("recentFileList").toStringList();

    QList<QAction *> recentFileActs = getOpenRecentFileActions();
    int numRecentFiles = qMin(files.size(), recentFileActs.length());

    for (int i = 0; i < numRecentFiles; ++i) {
        QString text = tr("&%1 %2").arg(i + 1).arg( QFileInfo(files[i]).fileName());
        recentFileActs[i]->setText(text);
        recentFileActs[i]->setData(files[i]);
        recentFileActs[i]->setVisible(true);
    }
    for (int j = numRecentFiles; j < recentFileActs.length(); ++j) {
        recentFileActs[j]->setVisible(false);
    }
}


void ActionFactory::onFileOpened(QFile &openedFile)
{
    QString fileName = openedFile.fileName();
    QSettings settings;
    QStringList files = settings.value("recentFileList").toStringList();
    files.removeAll(fileName);
    files.prepend(fileName);
    while (files.size() > getOpenRecentFileActions().length() )
        files.removeLast();
    settings.setValue("recentFileList", files);

    updateRecentFilesActions();
}


QAction *ActionFactory::getAction(const QString &actionId)
{
    QAction *result = nullptr;
    foreach(QAction *result, actions ) {
       if(result->objectName() == actionId) {
           return result;
       }
    }
    return nullptr;
}


QList<QAction *> ActionFactory::getOpenRecentFileActions()
{
    return getActionsStartingWith(DOCUMENT_OPEN_RECENT + "#");
}


QList<QAction *>& ActionFactory::getActionsStartingWith(const QString &actionIdStart)
{
    QList<QAction *> *result = new QList<QAction *> ();
    foreach(QAction *action, actions ) {
       if(action->objectName().startsWith(actionIdStart)) {
           result->append(action);
       }
    }
    return *result;
}
