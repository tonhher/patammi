#ifndef MENUBUILDER_H
#define MENUBUILDER_H

#include <QMenu>

#include "ActionFactory.h"
#include <QMenuBar>


class MenuBuilder
{

public:
    explicit MenuBuilder(ActionFactory *actionFactory, QMenuBar *menuBar, QWidget *parent);

    void build();


private:
    ActionFactory *actionFactory;
    QMenuBar *menuBar;

    void buildMenu(QString actionPrefix, QStringList menuActions);

    void buildFileMenu();
    void buildEditMenu();
    void buildAboutMenu();
};

#endif // MENUBUILDER_H
