#ifndef ACTIONFACTORY_H
#define ACTIONFACTORY_H

#include <QObject>
#include <QAction>
#include <QString>

#include <QFile>

namespace ActionsNameNS {
    QString const ADDRESS_BOOK_NEW("address-book-new");
    QString const APPLICATION_EXIT("application-exit");
    QString const APPOINTMENT_NEW("appointment-new");
    QString const CALL_START("call-start");
    QString const CALL_STOP("call-stop");
    QString const CONTACT_NEW("contact-new");
    QString const DOCUMENT_NEW("document-new");
    QString const DOCUMENT_OPEN("document-open");
    QString const DOCUMENT_OPEN_RECENT("document-open-recent");
    QString const DOCUMENT_PAGE_SETUP("document-page-setup");
    QString const DOCUMENT_PRINT("document-print");
    QString const DOCUMENT_PRINT_PREVIEW("document-print-preview");
    QString const DOCUMENT_PROPERTIES("document-properties");
    QString const DOCUMENT_REVERT("document-revert");
    QString const DOCUMENT_SAVE("document-save");
    QString const DOCUMENT_SAVE_AS("document-save-as");
    QString const DOCUMENT_SEND("document-send");
    QString const EDIT_CLEAR("edit-clear");
    QString const EDIT_COPY("edit-copy");
    QString const EDIT_CUT("edit-cut");
    QString const EDIT_DELETE("edit-delete");
    QString const EDIT_FIND("edit-find");
    QString const EDIT_FIND_REPLACE("edit-find-replace");
    QString const EDIT_PASTE("edit-paste");
    QString const EDIT_REDO("edit-redo");
    QString const EDIT_SELECT_ALL("edit-select-all");
    QString const EDIT_UNDO("edit-undo");
    QString const FOLDER_NEW("folder-new");
    QString const FORMAT_INDENT_LESS("format-indent-less");
    QString const FORMAT_INDENT_MORE("format-indent-more");
    QString const FORMAT_JUSTIFY_CENTER("format-justify-center");
    QString const FORMAT_JUSTIFY_FILL("format-justify-fill");
    QString const FORMAT_JUSTIFY_LEFT("format-justify-left");
    QString const FORMAT_JUSTIFY_RIGHT("format-justify-right");
    QString const FORMAT_TEXT_DIRECTION_LTR("format-text-direction-ltr");
    QString const FORMAT_TEXT_DIRECTION_RTL("format-text-direction-rtl");
    QString const FORMAT_TEXT_BOLD("format-text-bold");
    QString const FORMAT_TEXT_ITALIC("format-text-italic");
    QString const FORMAT_TEXT_UNDERLINE("format-text-underline");
    QString const FORMAT_TEXT_STRIKETHROUGH("format-text-strikethrough");
    QString const GO_BOTTOM("go-bottom");
    QString const GO_DOWN("go-down");
    QString const GO_FIRST("go-first");
    QString const GO_HOME("go-home");
    QString const GO_JUMP("go-jump");
    QString const GO_LAST("go-last");
    QString const GO_NEXT("go-next");
    QString const GO_PREVIOUS("go-previous");
    QString const GO_TOP("go-top");
    QString const GO_UP("go-up");
    QString const HELP_ABOUT("help-about");
    QString const HELP_CONTENTS("help-contents");
    QString const HELP_FAQ("help-faq");
    QString const INSERT_IMAGE("insert-image");
    QString const INSERT_LINK("insert-link");
    QString const INSERT_OBJECT("insert-object");
    QString const INSERT_TEXT("insert-text");
    QString const LIST_ADD("list-add");
    QString const LIST_REMOVE("list-remove");
    QString const MAIL_FORWARD("mail-forward");
    QString const MAIL_MARK_IMPORTANT("mail-mark-important");
    QString const MAIL_MARK_JUNK("mail-mark-junk");
    QString const MAIL_MARK_NOTJUNK("mail-mark-notjunk");
    QString const MAIL_MARK_READ("mail-mark-read");
    QString const MAIL_MARK_UNREAD("mail-mark-unread");
    QString const MAIL_MESSAGE_NEW("mail-message-new");
    QString const MAIL_REPLY_ALL("mail-reply-all");
    QString const MAIL_REPLY_SENDER("mail-reply-sender");
    QString const MAIL_SEND("mail-send");
    QString const MAIL_SEND_RECEIVE("mail-send-receive");
    QString const MEDIA_EJECT("media-eject");
    QString const MEDIA_PLAYBACK_PAUSE("media-playback-pause");
    QString const MEDIA_PLAYBACK_START("media-playback-start");
    QString const MEDIA_PLAYBACK_STOP("media-playback-stop");
    QString const MEDIA_RECORD("media-record");
    QString const MEDIA_SEEK_BACKWARD("media-seek-backward");
    QString const MEDIA_SEEK_FORWARD("media-seek-forward");
    QString const MEDIA_SKIP_BACKWARD("media-skip-backward");
    QString const MEDIA_SKIP_FORWARD("media-skip-forward");
    QString const OBJECT_FLIP_HORIZONTAL("object-flip-horizontal");
    QString const OBJECT_FLIP_VERTICAL("object-flip-vertical");
    QString const OBJECT_ROTATE_LEFT("object-rotate-left");
    QString const OBJECT_ROTATE_RIGHT("object-rotate-right");
    QString const PROCESS_STOP("process-stop");
    QString const SYSTEM_LOCK_SCREEN("system-lock-screen");
    QString const SYSTEM_LOG_OUT("system-log-out");
    QString const SYSTEM_RUN("system-run");
    QString const SYSTEM_SEARCH("system-search");
    QString const SYSTEM_REBOOT("system-reboot");
    QString const SYSTEM_SHUTDOWN("system-shutdown");
    QString const TOOLS_CHECK_SPELLING("tools-check-spelling");
    QString const VIEW_FULLSCREEN("view-fullscreen");
    QString const VIEW_REFRESH("view-refresh");
    QString const VIEW_RESTORE("view-restore");
    QString const VIEW_SORT_ASCENDING("view-sort-ascending");
    QString const VIEW_SORT_DESCENDING("view-sort-descending");
    QString const WINDOW_CLOSE("window-close");
    QString const WINDOW_NEW("window-new");
    QString const ZOOM_FIT_BEST("zoom-fit-best");
    QString const ZOOM_IN("zoom-in");
    QString const ZOOM_ORIGINAL("zoom-original");
    QString const ZOOM_OUT("zoom-out");
    QString const ANIMATION("Animation");
    QString const ICONS("Icons");

    /** Out of standard **/
    QString const HELP_ABOUT_QT("help-about-qt");
    QString const DOCUMENT_OPEN_RECENT_1("document-open-recent#1");
    QString const DOCUMENT_OPEN_RECENT_2("document-open-recent#2");
    QString const DOCUMENT_OPEN_RECENT_3("document-open-recent#3");
    QString const DOCUMENT_OPEN_RECENT_4("document-open-recent#4");

}

/***
 * This class provides a standard set of Action commonly used on gui
 * based upon the following specification:
 *      https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
 *
 *
 * */
class ActionFactory : public QObject
{


private :
    Q_OBJECT

public:

    explicit ActionFactory(QObject *parent = 0);
    void initialize(void);

    void addAction(QString id, QAction *actionToAdd);
    /***
     * Returns the QAction associated with provided ActionId.
     * Note that multiple calls with the same ActionId always returns the same object
     * */
    QAction * getAction(const QString &actionId);
    QList<QAction *> getOpenRecentFileActions();


public slots:
    void onFileOpened(QFile &openedFile);

signals:


private :
    QList<QAction *> actions;

    bool build = false;

    void createAllActions();
    void createConnections();
    QList<QAction *>& getActionsStartingWith(const QString &actionIdStart);

    QAction * createAction(const QString &id,
            const QString &text,
            const QIcon icon,
            const QKeySequence &shortcut,
            const QString &tooltip);

    void updateRecentFilesActions();

private slots:



};

#endif // ACTIONFACTORY_H
