#ifndef EACTIONID_H
#define EACTIONID_H

enum EActionId {

    ACTION_NEW,
    ACTION_OPEN,
    ACTION_CLOSE,

    MAX_E_ACTION_ID


}

#endif // EACTIONID_H
