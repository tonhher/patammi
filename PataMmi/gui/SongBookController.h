#ifndef SONGBOOKCONTROLLER_H
#define SONGBOOKCONTROLLER_H

#include <QObject>
#include "MainWindow.h"
#include "Book.h"
#include "Song.h"

/***
 *
 * @brief The SongBookController class
 *
 * This class is in charge of processing actions involved on a SongBook edition including UI refresh.
 * Each available actions is wired to a slot to be called through signals
 * These actions can be raised from UI or a batch mode
 *
 */
class SongBookController : public QObject
{
    Q_OBJECT

public:
    SongBookController();

    // Provide the view (the UI to linked to the edited data)
    void setView(MainWindow *view);

    // These signals are emitted on events produced by this controller. By now, they are mostly unused
    // Remenber that UI is directly refreshed by this class
signals:
   void bookOpened(Book *songBook);
   void bookClosed(Book *songBook);
   void songOpened(Song *newSong);
   void songAdded(Song *newSong);
   void songRemoved(Song *deprecatedSong);

   // The following slots stand for service that can be launched by this controller
   // These slots are mostly aimed to be launched from UI signals but they are designed
   // to be UI free ( can be launch in batch mode from command line for example)
public slots:
   void openBook(QFile &songBook) ;
   void saveBook();
   void closeBook();

   void openSong(Song *song);
   void closeSong(Song *song);
   void addSong(Song *newSong);
   void removeSong(Song *deprecatedSong);

private:
       Book *book;
       MainWindow *view;
};

#endif // SONGBOOKCONTROLLER_H
