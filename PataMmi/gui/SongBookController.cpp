#include "SongBookController.h"

#include "Logger.h"
#include "DataModel.h"
#include "BookModelBuilder.h"
#include "JsonBook.h"

SongBookController::SongBookController()
{
    book = Q_NULLPTR;
}

void SongBookController::setView(MainWindow *view)
{
    this->view = view;

    connect( view, SIGNAL(openSongBookRequest(QFile&)),
                this, SLOT(openBook(QFile&)) );

}



void SongBookController::saveBook()
{

}

void SongBookController::openBook(QFile &songBookFile)
{
    Book *songBook = new JsonBook(songBookFile);
    if( book != Q_NULLPTR ) {
        if(book->getDataDir().compare(songBook->getDataDir()) != 0 ) {
            closeBook();
        } else {
            LOG_WARNING("Book " + book->getDataDir() + "is already loaded");
        }
    }

    book = songBook;
    DataModel *smodel = (new BookModelBuilder())->build(songBook);
    view->setModel(smodel->getFilterProxy(), QModelIndex(),smodel->getModelDictionnary());
    emit bookOpened(songBook);

}
void SongBookController::closeBook()
{

}

void SongBookController::openSong(Song *song)
{

}



void SongBookController::addSong(Song *newSong)
{

}

void SongBookController::closeSong(Song *song)
{

}
void SongBookController::removeSong(Song *deprecatedSong)
{

}


