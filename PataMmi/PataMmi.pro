#-------------------------------------------------
#
# Project created by QtCreator 2016-05-08T23:12:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PataMmi
TEMPLATE = app

#  Allow C++14 Support
#QMAKE_CXXFLAGS += -std=c++14
#CONFIG += c++14

SOURCES += main.cpp\
        MainWindow.cpp \
    SongBook/Book.cpp \
    SongBook/BookModelBuilder.cpp \
    SongBook/JsonBook.cpp \
    SongBook/LatexUtils.cpp \
    SongBook/Song.cpp \
    SongBook/SongFactory.cpp \
    model/DataModel.cpp \
    gui/ActionFactory.cpp \
    Song/SongDocument.cpp \
    Song/CodeEditor.cpp \
    gui/MenuBuilder.cpp \
    gui/SongBookController.cpp

HEADERS  += MainWindow.h \
    SongBook/Book.h \
    SongBook/BookModelBuilder.h \
    SongBook/JsonBook.h \
    SongBook/LatexUtils.h \
    SongBook/Song.h \
    SongBook/SongFactory.h \
    ui_MainWindow.h \
    model/DataModel.h \
    gui/ActionFactory.h \
    Song/SongDocument.h \
    Song/CodeEditor.h \
    ui_SongEditor.h \
    gui/MenuBuilder.h \
    gui/SongBookController.h

INCLUDEPATH += ../CuteLogger/include \
    SongBook \
    model   \
    gui

FORMS    += MainWindow.ui \
    SongEditor.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../CuteLogger/ -lLogger
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../CuteLogger/ -lLoggerd
else:unix: LIBS += -L$$OUT_PWD/../CuteLogger/ -lLogger

INCLUDEPATH += $$PWD/../CuteLogger
DEPENDPATH += $$PWD/../CuteLogger

RESOURCES += \
    actions.qrc
