#include "MainWindow.h"
#include <QApplication>
#include <QFileSystemModel>

#include "Logger.h"
#include <ConsoleAppender.h>


// FOR DEBUG ONLY -----------
#include "SongFactory.h"
#include "Song.h"
#include "JsonBook.h"
#include "SongBookController.h"

// ---------------------------

int main(int argc, char *argv[])
{
    ConsoleAppender* consoleAppender = new ConsoleAppender;
    // consoleAppender->setFormat("[%{type:-7}] <%{Function}> %{message}\n");
    logger->registerAppender(consoleAppender);
    LOG_INFO("Starting the application");

    QApplication a(argc, argv);
    MainWindow *window = new MainWindow();

    SongBookController *controller = new SongBookController();
    controller->setView(window);

    /** For debug: automatically open a SongBook */
   // QFile hardCodedBookFile("/home/tonio/workspaces/patadata/books/volume-1.sb");
   // controller->openBook(hardCodedBookFile);
    /**** */

    window->show();
    return a.exec();


    /****
     * TODO application:
     *  - read/save book using json format
     *  - read/save book preferences
     *  - parse a directory to identify existing songs
     * ****/

    /***
     * TODO archi:
     * - status bar messages, make console output
     *
     *
     * */


    /****
     * TODO MMI:
     *  - getSelectedSongs
     *  - pop-up menu
     *  - action on click
     *  - status bar
     *  - Chords dock with predefined standards chords
     * ***/

    /****
     * TODO application:
     *  - read/save book using json format
     *  - read/save book preferences
     *  - parse a directory to identify existing songs
     * ****/

    /****
     * TODO improvment:
     *  - wire glyrc to download cover, lyric
     *  - wire frescobaldi
     * ***/

    /****
     * TODO improvment:
     *  - set-up a detacheableQtabWidget
     *  - wire frescobaldi
     * ***/
}


