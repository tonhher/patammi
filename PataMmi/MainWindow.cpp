#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QCompleter>
#include <QFileDialog>
#include <QMessageBox>

#include "BookModelBuilder.h"
#include "DataModel.h"
#include "MenuBuilder.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    actionFactory(this),
    ui(new Ui::MainWindow) {

    ui->setupUi(this);

    // actionFactory.setProjectDocumentName(QStringLiteral("SongBook"));
    actionFactory.initialize();

    setupTableView(ui->tableView);
    setupMenu(ui->menuBar);
    setUpInternalActions();
}

MainWindow::~MainWindow() {

    delete ui->searchEdit->completer();
    delete ui;
}

void MainWindow::about() {
    QMessageBox::about(this, tr("About PataMii"),
               tr("The <b>PataMii</b> is a Patacrep songbook editor. \nVersion 0.1"));
}


ActionFactory * MainWindow::getActionFactory() {
    return &actionFactory;
}

void MainWindow::setModel(QAbstractItemModel *model, const QModelIndex &rootIndex, const QStringList modelDictionnary ){

    ui->columnView->setModel(model);
    ui->columnView->setRootIndex(rootIndex);

    ui->tableView->setModel(model);
    ui->tableView->setRootIndex(rootIndex);

    connect(ui->searchEdit, SIGNAL(textChanged(QString)),
            model, SLOT(setFilterRegExp(QString)) );

    /* This completer allow to quickly find an item in the list by proposing all existing words */
    QCompleter *completer = new QCompleter(modelDictionnary);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    ui->searchEdit->setCompleter(completer);
}

/**
 * @brief MainWindow::setupTableView
 *
 * Setup the table view visual aspect
 *
 */
void MainWindow::setupTableView(QTableView * tableViewHoldingSongList) {
  tableViewHoldingSongList->setSortingEnabled(true);
  tableViewHoldingSongList->setAlternatingRowColors(true);ui->tableView->verticalHeader()->hide();
  tableViewHoldingSongList->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
  tableViewHoldingSongList->setSelectionBehavior(QAbstractItemView::SelectRows);
  tableViewHoldingSongList->setEditTriggers(QAbstractItemView::NoEditTriggers);
  // TODO: Check/Uncheck selected lines on [SPACE] key action
  tableViewHoldingSongList->actions();

  setupTableViewContextualMenu(tableViewHoldingSongList);
}

void MainWindow::setupTableViewContextualMenu(QTableView *tableViewHoldingSongList)
{
    // TODO ....
}

void MainWindow::setupMenu(QMenuBar *menuBar)
{
    MenuBuilder menuBuilder(&actionFactory, menuBar, this );
    menuBuilder.build();
}

void MainWindow::setUpInternalActions()
{

    connect( actionFactory.getAction(ActionsNameNS::DOCUMENT_OPEN), SIGNAL(triggered()),
            this, SLOT(onOpenSongBookAction()));

    // Inform ActionFactory about a recently opened file in order to update its recent files action list
    connect( this, SIGNAL(openSongBookRequest(QFile&)),
             &actionFactory, SLOT(onFileOpened(QFile&)));

    foreach(QAction *openRecentAction , actionFactory.getOpenRecentFileActions() ) {
        connect( openRecentAction , SIGNAL(triggered()),
                 this, SLOT(onOpenRecentSongBookAction()));
    }


    connect(actionFactory.getAction(ActionsNameNS::HELP_ABOUT), SIGNAL(triggered()), this, SLOT(about()));



}

void MainWindow::onOpenSongBookAction()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                  tr("Open SongBook"), "", tr("SongBook Files (*.sb)"));
    if( !fileName.isEmpty() ) {
        QFile file(fileName);
        emit openSongBookRequest(file);
    }
}

void MainWindow::onOpenRecentSongBookAction() {

    QAction *action = qobject_cast<QAction *>(sender());
    if (action) {
        QFile file(action->data().toString());
        emit openSongBookRequest(file);
    }
}






