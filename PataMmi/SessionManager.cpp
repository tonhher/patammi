#include "SessionManager.h"
#include "Logger.h"


SessionManager::SessionManager(ActionFactory *factory, QObject *parent) : QObject(parent)
{
    this->factory = factory;
    book = NULL;
}

void SessionManager::openBook(Book *songBook) {
    if( book == NULL || book->getDataDir().compare(songBook->getDataDir()) != 0) {
        book =songBook;
        emit bookOpened(songBook);
    } else {
        LOG_INFO("Book " + book->getDataDir() + "is already loaded");
    }

}
