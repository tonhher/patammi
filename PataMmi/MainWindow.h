#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSortFilterProxyModel>
#include <QTableView>
#include <QFile>

#include "DataModel.h"
#include "Book.h"
#include "ActionFactory.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    ActionFactory * getActionFactory();

    /***
     * Provides the SongBook ItemModelIndex to dsplay and its associated dictionary (to speed up search fucntion)
     * */
    void setModel(QAbstractItemModel *model, const QModelIndex &rootIndex, const QStringList modelDictionnary );


signals:
    void openSongBookRequest(QFile &songBookFile);

private:
    Ui::MainWindow *ui;
    ActionFactory actionFactory;
    void setupTableView(QTableView * tableViewHoldingSongList);
    void setupTableViewContextualMenu(QTableView * tableViewHoldingSongList);
    void setupMenu(QMenuBar *menuBar);
    void setUpInternalActions();

private slots:
    void onOpenSongBookAction();
    void onOpenRecentSongBookAction();
    void about();
};

#endif // MAINWINDOW_H
