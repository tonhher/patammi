#ifndef SONGHEADEREDITOR_H
#define SONGHEADEREDITOR_H


namespace Ui {
class SongHeaderEditor;
}

class SongHeaderEditor : public QWidget
{
    Q_OBJECT

public:
    explicit SongHeaderEditor(QWidget *parent = 0);
    ~SongHeaderEditor();

private:
    Ui::SongHeaderEditor *ui;
};

#endif // SONGHEADEREDITOR_H
