#include "SongHeaderEditor.h"
#include "ui_SongHeaderEditor.h"

SongHeaderEditor::SongHeaderEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SongHeaderEditor)
{
    ui->setupUi(this);
}

SongHeaderEditor::~SongHeaderEditor()
{
    delete ui;
}
